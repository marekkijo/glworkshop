#ifndef NORMAL_MAPPING_RENDERER_H
#define NORMAL_MAPPING_RENDERER_H

#ifdef _WIN32
# include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "glcpl/renderer.hpp"

class NormalMappingRenderer : public glcpl::Renderer {
public:
    explicit NormalMappingRenderer(std::shared_ptr<glcpl::Context> context);
    ~NormalMappingRenderer() override;

    void attachEvent() override;
    void resizeEvent(const glcpl::ResizeEventArgs &args) override;
    void keyEvent(const glcpl::KeyEventArgs &args) override;
    void timerEvent(const glcpl::TimerEventArgs &args) override;

private:
    bool _initialized{false};
    GLuint _redProgram{0u};
    GLint _mvpLocation{0u};
    GLuint _triangleVAO{0u};
    float _zRot{0.0f};

    bool loadGLExtensions();
    bool initShaders();
};

#endif // NORMAL_MAPPING_RENDERER_H
