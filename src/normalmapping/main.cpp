#include "glcpl/window.hpp"
#include "glcpl/context.hpp"
#include "normalmappingrerenderer.hpp"

int main(int argc, char *argv[]) {
    glcpl::WindowInfo windowInfo;
    windowInfo.name = "NormalMapping";
    windowInfo.type = glcpl::WindowInfo::Type::Borderless;
    windowInfo.width = 640;
    windowInfo.height = 480;
    windowInfo.posx = 0;
    windowInfo.posy = 0;
    windowInfo.centered = true;
    windowInfo.exitOnEscape = true;
    windowInfo.cursorVisible = true;
    windowInfo.cursorCentering = false;

    auto window = glcpl::Window::create(windowInfo);
    if (!window)
        return 1;

    auto context = window->createContext();

    auto renderer = std::make_shared<NormalMappingRenderer>(context);

    window->attachRenderer(renderer);

    window->setTimerInterval(0);

    return window->exec();
}
