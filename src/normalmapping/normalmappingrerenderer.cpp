#include "normalmappingrerenderer.hpp"
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glcpl/context.hpp"

NormalMappingRenderer::NormalMappingRenderer(std::shared_ptr<glcpl::Context> context) :
    glcpl::Renderer(context) {
}

NormalMappingRenderer::~NormalMappingRenderer() {
}

void NormalMappingRenderer::attachEvent() {
    _context->makeCurrent();

    if (_initialized)
        return;

    if (!loadGLExtensions())
        return;

    if (!initShaders())
        return;

    GLfloat vertices[] = {
           0.0f,  1.0f, 0.0f,
        -0.866f, -0.5f, 0.0f,
         0.866f, -0.5f, 0.0f
    };

    GLuint vertexBufferID = 0u;
    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glGenVertexArrays(1, &_triangleVAO);
    glBindVertexArray(_triangleVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, static_cast<void *>(0x0));
    glBindVertexArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    _initialized = true;
}

void NormalMappingRenderer::resizeEvent(const glcpl::ResizeEventArgs &args) {
    glViewport(0, 0, args.width, args.height);
}

void NormalMappingRenderer::keyEvent(const glcpl::KeyEventArgs &args) {
    std::cout << std::hex;
    if (args.isDown)
        std::cout << "keyEvent: pressed " <<
        ((args.keyModifier & glcpl::KeyModifier::Control) != glcpl::KeyModifier::None ? "Ctrl + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Alt) != glcpl::KeyModifier::None ? "Alt + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Shift) != glcpl::KeyModifier::None ? "Shift + " : "") <<
        "0x" << static_cast<unsigned int>(args.keyCode) << std::endl;
    else
        std::cout << "keyEvent: released " <<
        ((args.keyModifier & glcpl::KeyModifier::Control) != glcpl::KeyModifier::None ? "Ctrl + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Alt) != glcpl::KeyModifier::None ? "Alt + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Shift) != glcpl::KeyModifier::None ? "Shift + " : "") <<
        "0x" << static_cast<unsigned int>(args.keyCode) << std::endl;
}

void NormalMappingRenderer::timerEvent(const glcpl::TimerEventArgs &args) {
    glm::mat4 mv = glm::rotate(glm::mat4(1.0f), _zRot, glm::vec3(0.0f, 0.0f, 1.0f));
    _zRot += static_cast<float>(args.elapsed) / 1000.0f;

    glUniformMatrix4fv(_mvpLocation, 1, GL_FALSE, glm::value_ptr(mv));

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(_redProgram);

    glBindVertexArray(_triangleVAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    _context->swapBuffers();
}

bool NormalMappingRenderer::loadGLExtensions() {
    GLenum success = glewInit();

    if (success != GLEW_OK)
        return false;

    return true;
}

bool NormalMappingRenderer::initShaders() {
    GLint success = GL_FALSE;

    GLuint vsID = glCreateShader(GL_VERTEX_SHADER);
    const char *vs = {"\
#version 330 core\n\n\
uniform mat4 mvp;\n\
layout(location = 0) in vec3 vertex;\n\
\n\
void main() {\n\
    gl_Position = mvp * vec4(vertex, 1.0);\n\
}\n"};
    glShaderSource(vsID, 1, &vs, nullptr);
    glCompileShader(vsID);
    glGetShaderiv(vsID, GL_COMPILE_STATUS, &success);
    if (success != GL_TRUE) {
        GLint logLength = 0;
        glGetShaderiv(vsID, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> log(logLength);
        glGetShaderInfoLog(vsID, logLength, nullptr, log.data());
        std::cout << "VS compile error:\n" << log.data() << std::endl;

        glDeleteShader(vsID);

        return false;
    }

    GLuint fsID = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs = {"\
#version 330 core\n\n\
const vec4 color = vec4(1.0, 0.0, 0.0, 1.0);\n\
\n\
void main() {\n\
    gl_FragColor = color;\n\
}\n"};
    glShaderSource(fsID, 1, &fs, nullptr);
    glCompileShader(fsID);
    glGetShaderiv(fsID, GL_COMPILE_STATUS, &success);
    if (success != GL_TRUE) {
        GLint logLength = 0;
        glGetShaderiv(fsID, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> log(logLength);
        glGetShaderInfoLog(fsID, logLength, nullptr, log.data());
        std::cout << "FS compile error:\n" << log.data() << std::endl;

        glDeleteShader(vsID);
        glDeleteShader(fsID);

        return false;
    }

    _redProgram = glCreateProgram();

    glAttachShader(_redProgram, vsID);
    glAttachShader(_redProgram, fsID);

    glLinkProgram(_redProgram);
    glGetProgramiv(_redProgram, GL_LINK_STATUS, &success);
    if (success != GL_TRUE) {
        GLint logLength = 0;
        glGetProgramiv(_redProgram, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> log(logLength);
        glGetProgramInfoLog(_redProgram, logLength, nullptr, log.data());
        std::cout << "Link error:\n" << log.data() << std::endl;

        glDeleteShader(_redProgram);
        _redProgram = 0u;

        glDeleteShader(vsID);
        glDeleteShader(fsID);

        return false;
    }

    glDetachShader(_redProgram, vsID);
    glDetachShader(_redProgram, fsID);

    glDeleteShader(vsID);
    glDeleteShader(fsID);

    _mvpLocation = glGetUniformLocation(_redProgram, "mvp");

    return true;
}
