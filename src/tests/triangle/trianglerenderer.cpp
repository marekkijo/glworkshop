#include "trianglerenderer.hpp"
#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include "glcpl/context.hpp"

#ifndef APIENTRY
# define APIENTRY
#endif // !APIENTRY

using fp_glCreateShader = GLuint(APIENTRY *)(GLenum shaderType);
static fp_glCreateShader glCreateShader{nullptr};
using fp_glDeleteShader = void(APIENTRY *)(GLuint shader);
static fp_glDeleteShader glDeleteShader{nullptr};
using fp_glShaderSource = void(APIENTRY *)(GLuint shader, GLsizei count, const GLchar **string, const GLint *length);
static fp_glShaderSource glShaderSource{nullptr};
using fp_glCompileShader = void(APIENTRY *)(GLuint shader);
static fp_glCompileShader glCompileShader{nullptr};
using fp_glAttachShader = void(APIENTRY *)(GLuint program, GLuint shader);
static fp_glAttachShader glAttachShader{nullptr};
using fp_glDetachShader = void(APIENTRY *)(GLuint program, GLuint shader);
static fp_glDetachShader glDetachShader{nullptr};
using fp_glGetShaderiv = void(APIENTRY *)(GLuint shader, GLenum pname, GLint *params);
static fp_glGetShaderiv glGetShaderiv{nullptr};
using fp_glGetShaderInfoLog = void(APIENTRY *)(GLuint shader, GLsizei maxLength, GLsizei *length, GLchar *infoLog);
static fp_glGetShaderInfoLog glGetShaderInfoLog{nullptr};

using fp_glCreateProgram = GLuint(APIENTRY *)();
static fp_glCreateProgram glCreateProgram{nullptr};
using fp_glDeleteProgram = void(APIENTRY *)(GLuint program);
static fp_glDeleteProgram glDeleteProgram{nullptr};
using fp_glLinkProgram = void(APIENTRY *)(GLuint program);
static fp_glLinkProgram glLinkProgram{nullptr};
using fp_glUseProgram = void(APIENTRY *)(GLuint program);
static fp_glUseProgram glUseProgram{nullptr};
using fp_glGetProgramiv = void(APIENTRY *)(GLuint program, GLenum pname, GLint *params);
static fp_glGetProgramiv glGetProgramiv{nullptr};
using fp_glGetProgramInfoLog = void(APIENTRY *)(GLuint program, GLsizei maxLength, GLsizei *length, GLchar *infoLog);
static fp_glGetProgramInfoLog glGetProgramInfoLog{nullptr};

using fp_glGenVertexArrays = void(APIENTRY *)(GLsizei n, GLuint *arrays);
static fp_glGenVertexArrays glGenVertexArrays{nullptr};
using fp_glDeleteVertexArrays = void(APIENTRY *)(GLsizei n, const GLuint *arrays);
static fp_glDeleteVertexArrays glDeleteVertexArrays{nullptr};
using fp_glBindVertexArray = void(APIENTRY *)(GLuint array);
static fp_glBindVertexArray glBindVertexArray{nullptr};
using fp_glEnableVertexAttribArray = void(APIENTRY *)(GLuint index);
static fp_glEnableVertexAttribArray glEnableVertexAttribArray{nullptr};
using fp_glDisableVertexAttribArray = void(APIENTRY *)(GLuint index);
static fp_glDisableVertexAttribArray glDisableVertexAttribArray{nullptr};
using fp_glVertexAttribPointer = void(APIENTRY *)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);
static fp_glVertexAttribPointer glVertexAttribPointer{nullptr};
using fp_glVertexAttribIPointer = void(APIENTRY *)(GLuint index, GLint size, GLenum type, GLsizei stride, const GLvoid *pointer);
static fp_glVertexAttribIPointer glVertexAttribIPointer{nullptr};
using fp_glVertexAttribLPointer = void(APIENTRY *)(GLuint index, GLint size, GLenum type, GLsizei stride, const GLvoid *pointer);
static fp_glVertexAttribLPointer glVertexAttribLPointer{nullptr};

using fp_glGenBuffers = void(APIENTRY *)(GLsizei n, GLuint *buffers);
static fp_glGenBuffers glGenBuffers{nullptr};
using fp_glDeleteBuffers = void(APIENTRY *)(GLsizei n, const GLuint *buffers);
static fp_glDeleteBuffers glDeleteBuffers{nullptr};
using fp_glBindBuffer = void(APIENTRY *)(GLenum target, GLuint buffer);
static fp_glBindBuffer glBindBuffer{nullptr};
using fp_glBufferData = void(APIENTRY *)(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
static fp_glBufferData glBufferData{nullptr};
using fp_glNamedBufferData = void(APIENTRY *)(GLuint buffer, GLsizei size, const void *data, GLenum usage);
static fp_glNamedBufferData glNamedBufferData{nullptr};

using fp_glGetUniformLocation = GLint(APIENTRY *)(GLuint program, const GLchar *name);
static fp_glGetUniformLocation glGetUniformLocation{nullptr};
using fp_glUniformMatrix4fv = void(APIENTRY *)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
static fp_glUniformMatrix4fv glUniformMatrix4fv{nullptr};

TriangleRenderer::TriangleRenderer(std::shared_ptr<glcpl::Context> context) :
    glcpl::Renderer(context) {
}

TriangleRenderer::~TriangleRenderer() {
}

void TriangleRenderer::attachEvent() {
    // TODO: Have to deal with context == nullptr passed to renderer constructor
    _context->makeCurrent();

    if (_initialized)
        return;

    if (!loadGLExtensions())
        return;

    if (!initShaders())
        return;

    GLfloat vertices[] = {
           0.0f,  1.0f, 0.0f,
        -0.866f, -0.5f, 0.0f,
         0.866f, -0.5f, 0.0f
    };

    GLuint vertexBufferID = 0u;
    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glGenVertexArrays(1, &_triangleVAO);
    glBindVertexArray(_triangleVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, static_cast<void *>(0x0));
    glBindVertexArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    _initialized = true;
}

void TriangleRenderer::resizeEvent(const glcpl::ResizeEventArgs &args) {
    glViewport(0, 0, args.width, args.height);
}

void TriangleRenderer::keyEvent(const glcpl::KeyEventArgs &args) {
    std::cout << std::hex;
    if (args.isDown)
        std::cout << "keyEvent: pressed " <<
        ((args.keyModifier & glcpl::KeyModifier::Control) != glcpl::KeyModifier::None ? "Ctrl + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Alt) != glcpl::KeyModifier::None ? "Alt + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Shift) != glcpl::KeyModifier::None ? "Shift + " : "") <<
        "0x" << static_cast<unsigned int>(args.keyCode) << std::endl;
    else
        std::cout << "keyEvent: released " <<
        ((args.keyModifier & glcpl::KeyModifier::Control) != glcpl::KeyModifier::None ? "Ctrl + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Alt) != glcpl::KeyModifier::None ? "Alt + " : "") <<
        ((args.keyModifier & glcpl::KeyModifier::Shift) != glcpl::KeyModifier::None ? "Shift + " : "") <<
        "0x" << static_cast<unsigned int>(args.keyCode) << std::endl;
}

void TriangleRenderer::timerEvent(const glcpl::TimerEventArgs &args) {
    std::array<GLfloat, 16> zRotMat{
        std::cos(_zRot), -std::sin(_zRot), 0, 0,
        std::sin(_zRot),  std::cos(_zRot), 0, 0,
                      0,                0, 1, 0,
                      0,                0, 0, 1
    };
    _zRot += static_cast<float>(args.elapsed) / 1000.0f;

    glUniformMatrix4fv(_mvpLocation, 1, GL_FALSE, zRotMat.data());

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(_redProgram);

    glBindVertexArray(_triangleVAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    _context->swapBuffers();
}

bool TriangleRenderer::loadGLExtensions() {
    glCreateShader = reinterpret_cast<fp_glCreateShader>(getGLProcedure("glCreateShader"));
    glDeleteShader = reinterpret_cast<fp_glDeleteShader>(getGLProcedure("glDeleteShader"));
    glShaderSource = reinterpret_cast<fp_glShaderSource>(getGLProcedure("glShaderSource"));
    glCompileShader = reinterpret_cast<fp_glCompileShader>(getGLProcedure("glCompileShader"));
    glAttachShader = reinterpret_cast<fp_glAttachShader>(getGLProcedure("glAttachShader"));
    glDetachShader = reinterpret_cast<fp_glDetachShader>(getGLProcedure("glDetachShader"));
    glGetShaderiv = reinterpret_cast<fp_glGetShaderiv>(getGLProcedure("glGetShaderiv"));
    glGetShaderInfoLog = reinterpret_cast<fp_glGetShaderInfoLog>(getGLProcedure("glGetShaderInfoLog"));
    glCreateProgram = reinterpret_cast<fp_glCreateProgram>(getGLProcedure("glCreateProgram"));
    glDeleteProgram = reinterpret_cast<fp_glDeleteProgram>(getGLProcedure("glDeleteProgram"));
    glLinkProgram = reinterpret_cast<fp_glLinkProgram>(getGLProcedure("glLinkProgram"));
    glUseProgram = reinterpret_cast<fp_glUseProgram>(getGLProcedure("glUseProgram"));
    glGetProgramiv = reinterpret_cast<fp_glGetProgramiv>(getGLProcedure("glGetProgramiv"));
    glGetProgramInfoLog = reinterpret_cast<fp_glGetProgramInfoLog>(getGLProcedure("glGetProgramInfoLog"));
    glGenVertexArrays = reinterpret_cast<fp_glGenVertexArrays>(getGLProcedure("glGenVertexArrays"));
    glDeleteVertexArrays = reinterpret_cast<fp_glDeleteVertexArrays>(getGLProcedure("glDeleteVertexArrays"));
    glBindVertexArray = reinterpret_cast<fp_glBindVertexArray>(getGLProcedure("glBindVertexArray"));
    glEnableVertexAttribArray = reinterpret_cast<fp_glEnableVertexAttribArray>(getGLProcedure("glEnableVertexAttribArray"));
    glDisableVertexAttribArray = reinterpret_cast<fp_glDisableVertexAttribArray>(getGLProcedure("glDisableVertexAttribArray"));
    glVertexAttribPointer = reinterpret_cast<fp_glVertexAttribPointer>(getGLProcedure("glVertexAttribPointer"));
    glVertexAttribIPointer = reinterpret_cast<fp_glVertexAttribIPointer>(getGLProcedure("glVertexAttribIPointer"));
    glVertexAttribLPointer = reinterpret_cast<fp_glVertexAttribLPointer>(getGLProcedure("glVertexAttribLPointer"));
    glGenBuffers = reinterpret_cast<fp_glGenBuffers>(getGLProcedure("glGenBuffers"));
    glDeleteBuffers = reinterpret_cast<fp_glDeleteBuffers>(getGLProcedure("glDeleteBuffers"));
    glBindBuffer = reinterpret_cast<fp_glBindBuffer>(getGLProcedure("glBindBuffer"));
    glBufferData = reinterpret_cast<fp_glBufferData>(getGLProcedure("glBufferData"));
    glNamedBufferData = reinterpret_cast<fp_glNamedBufferData>(getGLProcedure("glNamedBufferData"));
    glGetUniformLocation = reinterpret_cast<fp_glGetUniformLocation>(getGLProcedure("glGetUniformLocation"));
    glUniformMatrix4fv = reinterpret_cast<fp_glUniformMatrix4fv>(getGLProcedure("glUniformMatrix4fv"));

    if (!glCreateShader ||
        !glDeleteShader ||
        !glShaderSource ||
        !glCompileShader ||
        !glAttachShader ||
        !glDetachShader ||
        !glGetShaderiv ||
        !glGetShaderInfoLog ||
        !glCreateProgram ||
        !glDeleteProgram ||
        !glLinkProgram ||
        !glUseProgram ||
        !glGetProgramiv ||
        !glGetProgramInfoLog ||
        !glGenVertexArrays ||
        !glDeleteVertexArrays ||
        !glBindVertexArray ||
        !glEnableVertexAttribArray ||
        !glDisableVertexAttribArray ||
        !glVertexAttribPointer ||
        !glVertexAttribIPointer ||
        !glVertexAttribLPointer ||
        !glGenBuffers ||
        !glDeleteBuffers ||
        !glBindBuffer ||
        !glBufferData ||
        !glNamedBufferData ||
        !glGetUniformLocation ||
        !glUniformMatrix4fv) {
        glCreateShader = nullptr;
        glDeleteShader = nullptr;
        glShaderSource = nullptr;
        glCompileShader = nullptr;
        glAttachShader = nullptr;
        glDetachShader = nullptr;
        glGetShaderiv = nullptr;
        glGetShaderInfoLog = nullptr;
        glCreateProgram = nullptr;
        glDeleteProgram = nullptr;
        glLinkProgram = nullptr;
        glUseProgram = nullptr;
        glGetProgramiv = nullptr;
        glGetProgramInfoLog = nullptr;
        glGenVertexArrays = nullptr;
        glDeleteVertexArrays = nullptr;
        glBindVertexArray = nullptr;
        glEnableVertexAttribArray = nullptr;
        glDisableVertexAttribArray = nullptr;
        glVertexAttribPointer = nullptr;
        glVertexAttribIPointer = nullptr;
        glVertexAttribLPointer = nullptr;
        glGenBuffers = nullptr;
        glDeleteBuffers = nullptr;
        glBindBuffer = nullptr;
        glBufferData = nullptr;
        glNamedBufferData = nullptr;
        glGetUniformLocation = nullptr;
        glUniformMatrix4fv = nullptr;

        return false;
    }

    return true;
}

bool TriangleRenderer::initShaders() {
    GLint success = GL_FALSE;

    GLuint vsID = glCreateShader(GL_VERTEX_SHADER);
    const char *vs = {"\
#version 330 core\n\n\
uniform mat4 mvp;\n\
layout(location = 0) in vec3 vertex;\n\
\n\
void main() {\n\
    gl_Position = mvp * vec4(vertex, 1.0);\n\
}\n"};
    glShaderSource(vsID, 1, &vs, nullptr);
    glCompileShader(vsID);
    glGetShaderiv(vsID, GL_COMPILE_STATUS, &success);
    if (success != GL_TRUE) {
        GLint logLength = 0;
        glGetShaderiv(vsID, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> log(logLength);
        glGetShaderInfoLog(vsID, logLength, nullptr, log.data());
        if (logLength > 0) {
            std::cout << "VS compile error:\n" << log.data() << std::endl;
        }

        glDeleteShader(vsID);

        return false;
    }

    GLuint fsID = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs = {"\
#version 330 core\n\n\
const vec4 color = vec4(1.0, 0.0, 0.0, 1.0);\n\
\n\
void main() {\n\
    gl_FragColor = color;\n\
}\n"};
    glShaderSource(fsID, 1, &fs, nullptr);
    glCompileShader(fsID);
    glGetShaderiv(fsID, GL_COMPILE_STATUS, &success);
    if (success != GL_TRUE) {
        GLint logLength = 0;
        glGetShaderiv(fsID, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> log(logLength);
        glGetShaderInfoLog(fsID, logLength, nullptr, log.data());
        if (logLength > 0) {
            std::cout << "FS compile error:\n" << log.data() << std::endl;
        }

        glDeleteShader(vsID);
        glDeleteShader(fsID);

        return false;
    }

    _redProgram = glCreateProgram();

    glAttachShader(_redProgram, vsID);
    glAttachShader(_redProgram, fsID);

    glLinkProgram(_redProgram);
    glGetProgramiv(_redProgram, GL_LINK_STATUS, &success);
    if (success != GL_TRUE) {
        GLint logLength = 0;
        glGetProgramiv(_redProgram, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> log(logLength);
        glGetProgramInfoLog(_redProgram, logLength, nullptr, log.data());
        std::cout << "Link error:\n" << log.data() << std::endl;

        glDeleteShader(_redProgram);
        _redProgram = 0u;

        glDeleteShader(vsID);
        glDeleteShader(fsID);

        return false;
    }

    glDetachShader(_redProgram, vsID);
    glDetachShader(_redProgram, fsID);

    glDeleteShader(vsID);
    glDeleteShader(fsID);

    _mvpLocation = glGetUniformLocation(_redProgram, "mvp");

    return true;
}
