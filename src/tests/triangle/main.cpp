#include "glcpl/window.hpp"
#include "glcpl/context.hpp"
#include "trianglerenderer.hpp"

int main(int argc, char *argv[]) {
    glcpl::WindowInfo windowInfo;
    windowInfo.name = "Test: Triangle";
    windowInfo.type = glcpl::WindowInfo::Type::Borderless;
    windowInfo.width = 512;
    windowInfo.height = 512;
    windowInfo.posx = 0;
    windowInfo.posy = 0;
    windowInfo.centered = true;
    windowInfo.exitOnEscape = true;
    windowInfo.cursorVisible = true;
    windowInfo.cursorCentering = false;

    auto window = glcpl::Window::create(windowInfo);
    if (!window)
        return 1;

    auto context = window->createContext();

    auto renderer = std::make_shared<TriangleRenderer>(context);

    window->attachRenderer(renderer);

    window->setTimerInterval(0);

    return window->exec();
}
