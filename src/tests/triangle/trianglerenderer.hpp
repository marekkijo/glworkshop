#ifndef TRIANGLE_RENDERER_H
#define TRIANGLE_RENDERER_H

#ifdef _WIN32
# include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glext.h>

#include "glcpl/renderer.hpp"

class TriangleRenderer : public glcpl::Renderer {
public:
    explicit TriangleRenderer(std::shared_ptr<glcpl::Context> context);
    ~TriangleRenderer() override;

    void attachEvent() override;
    void resizeEvent(const glcpl::ResizeEventArgs &args) override;
    void keyEvent(const glcpl::KeyEventArgs &args) override;
    void timerEvent(const glcpl::TimerEventArgs &args) override;

private:
    bool _initialized{false};
    GLuint _redProgram{0u};
    GLint _mvpLocation{0u};
    GLuint _triangleVAO{0u};
    float _zRot{0.0f};

    bool loadGLExtensions();
    bool initShaders();
};

#endif // TRIANGLE_RENDERER_H
