#include "view.hpp"

#ifdef _WIN32
# include "win32/view_win32.hpp"
#endif

namespace glcpl {

View::View() {
}

#ifdef _WIN32
std::shared_ptr<View> View::create(HWND hWnd) {
    return std::make_shared<ViewWin32>(hWnd);
}
#else
static std::shared_ptr<View> View::create() {
    return nullptr;
}
#endif

}
