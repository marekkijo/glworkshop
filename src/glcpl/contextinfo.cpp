#include "contextinfo.hpp"

namespace glcpl {

uint8_t ContextInfo::getVersionMajor() const {
    if (version == Version::Undefined)
        return 0u;
    if (version == Version::Highest)
        return 1u;
    return static_cast<uint16_t>(version) >> 8u;
}

uint8_t ContextInfo::getVersionMinor() const {
    if (version == Version::Undefined || version == Version::Highest)
        return 0u;
    return static_cast<uint16_t>(version) & 0xFF;
}

ContextInfo::Version ContextInfo::toVersion(uint8_t major, uint8_t minor) {
    uint16_t versionValue = major;
    versionValue <<= 8u;
    versionValue &= minor;

    Version version = static_cast<Version>(versionValue);

    if (version != Version::V4_5 &&
        version != Version::V4_4 &&
        version != Version::V4_3 &&
        version != Version::V4_2 &&
        version != Version::V4_1 &&
        version != Version::V4_0 &&
        version != Version::V3_3 &&
        version != Version::V3_2 &&
        version != Version::V3_1 &&
        version != Version::V3_0 &&
        version != Version::V2_1 &&
        version != Version::V2_0 &&
        version != Version::V1_5 &&
        version != Version::V1_4 &&
        version != Version::V1_3 &&
        version != Version::V1_2 &&
        version != Version::V1_1)
        return Version::Undefined;

    return version;
}

}
