#include "window_win32.hpp"

namespace glcpl {

const std::string WindowWin32::s_windowClassName{"glcpl__WindowWin32"};
uint32_t WindowWin32::s_windowClassInstanceCounter{0u};
bool WindowWin32::s_windowClassRegistered{false};

WindowWin32::WindowWin32(const WindowInfo &windowInfo) :
    Window(windowInfo) {
    createWindowClass();

    _width = _windowInfo.width;
    _height = _windowInfo.height;
}

WindowWin32::~WindowWin32() {
    destroyWindowClass();
}

int WindowWin32::exec() {
    MSG msg{0u};

    while (GetMessage(&msg, _hWnd, 0, 0) != 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.message == WM_QUIT ? 0 : 1;
}

bool WindowWin32::init() {
    if (!s_windowClassRegistered)
        return false;

    DWORD dwStyle = WS_VISIBLE |
        (_windowInfo.type == WindowInfo::Type::Borderless ? WS_POPUP : WS_OVERLAPPEDWINDOW);

    uint32_t
        posx = _windowInfo.posx,
        posy = _windowInfo.posy;
    if (_windowInfo.centered) {
        posx = std::lround((GetSystemMetrics(SM_CXSCREEN) - _windowInfo.width) / 2.f);
        posy = std::lround((GetSystemMetrics(SM_CYSCREEN) - _windowInfo.height) / 2.f);
    }

    HINSTANCE hInstance = GetModuleHandle(nullptr);
    if (hInstance == NULL)
        return false;

    HWND hWnd = CreateWindow(
        s_windowClassName.c_str(),      // _In_opt_ LPCTSTR   lpClassName
        _windowInfo.name.c_str(),       // _In_opt_ LPCTSTR   lpWindowName
        dwStyle,                        // _In_     DWORD     dwStyle
        posx, posy,                     // _In_     int       x, y
        _windowInfo.width,              // _In_     int       nWidth
        _windowInfo.height,             // _In_     int       nHeight
        nullptr,                        // _In_opt_ HWND      hWndParent
        nullptr,                        // _In_opt_ HMENU     hMenu
        hInstance,                      // _In_opt_ HINSTANCE hInstance
        static_cast<LPVOID>(this)       // _In_opt_ LPVOID    lpParam
    );
    if (hWnd == NULL)
        return false;

    _hWnd = hWnd;

    refreshTimer();

    return true;
}

LRESULT WindowWin32::wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    // Intentionally do nothing - shouldn't be called on Window instance
    return LRESULT();
}

LRESULT WindowWin32::wndInternalProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
    case WM_CLOSE:
        DestroyWindow(_hWnd);
        return 0;
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE && _windowInfo.exitOnEscape) {
            DestroyWindow(_hWnd);
            return DefWindowProc(hWnd, msg, wParam, lParam);
        }
        return ViewWin32::wndProc(hWnd, msg, wParam, lParam);
    case WM_DESTROY:
        ViewWin32::wndProc(hWnd, msg, wParam, lParam);
        PostQuitMessage(0);
        return 0;
    default:
        return ViewWin32::wndProc(hWnd, msg, wParam, lParam);
    }
}

LRESULT WindowWin32::msgRouter(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    if (msg == WM_NCCREATE) {
        WindowWin32 *instance = reinterpret_cast<WindowWin32 *>(reinterpret_cast<LPCREATESTRUCT>(lParam)->lpCreateParams);

        if (!instance)
            return FALSE;
        SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(instance));

        return TRUE;
    } else {
        WindowWin32 *instance = reinterpret_cast<WindowWin32 *>(GetWindowLongPtr(hWnd, GWLP_USERDATA));

        if (instance)
            return instance->wndInternalProc(hWnd, msg, wParam, lParam);

        return DefWindowProc(hWnd, msg, wParam, lParam);
    }
}

void WindowWin32::createWindowClass() {
    if (s_windowClassInstanceCounter++ != 0)
        return;

    HINSTANCE hInstance = GetModuleHandle(nullptr);
    HICON hIcon = LoadIcon(nullptr, IDI_WINLOGO);
    HCURSOR hCursor = LoadCursor(nullptr, IDC_ARROW);
    HBRUSH hBrush = reinterpret_cast<HBRUSH>(COLOR_BACKGROUND);

    if (hInstance == NULL || hIcon == NULL || hCursor == NULL)
        return;

    WNDCLASS wndClass {
        CS_OWNDC | CS_DBLCLKS,      // UINT      style
        msgRouter,                  // WNDPROC   lpfnWndProc
        0,                          // int       cbClsExtra
        0,                          // int       cbWndExtra
        hInstance,                  // HINSTANCE hInstance
        hIcon,                      // HICON     hIcon
        hCursor,                    // HCURSOR   hCursor
        hBrush,                     // HBRUSH    hbrBackground
        nullptr,                    // LPCTSTR   lpszMenuName
        s_windowClassName.c_str()   // LPCTSTR   lpszClassName
    };

    s_windowClassRegistered = (RegisterClass(&wndClass) != 0);
}

void WindowWin32::destroyWindowClass() {
    if (--s_windowClassInstanceCounter != 0)
        return;

    UnregisterClass(
        s_windowClassName.c_str(),    // _In_     LPCTSTR   lpClassName
        GetModuleHandle(nullptr)      // _In_opt_ HINSTANCE hInstance
    );

    s_windowClassRegistered = false;
}

}
