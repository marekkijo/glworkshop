#ifndef GLCPL_VIEW_WIN32_H
#define GLCPL_VIEW_WIN32_H

#include <cstdint>
#include <memory>
#include <vector>
#include <array>
#include <windows.h>
#include "keys.hpp"
#include "view.hpp"

namespace glcpl {

class ContextWin32;

class ViewWin32 : public virtual View {
public:
    explicit ViewWin32(HWND hWnd = nullptr);
    ~ViewWin32() override;

    std::shared_ptr<Context> createContext(const ContextInfo &contextInfo) override;
    bool destroyContext(std::shared_ptr<Context> context) override;
    void attachRenderer(std::shared_ptr<Renderer> renderer) override;
    void setTimerInterval(uint32_t msec) override;
    void removeTimerInterval() override;
    LRESULT wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;

protected:
    HWND _hWnd{nullptr};
    std::shared_ptr<Renderer> _renderer;
    std::vector<std::shared_ptr<ContextWin32>> _contexts;
    uint32_t _width{0u};
    uint32_t _height{0u};
    int64_t _msecTimerInterval{-1};

    void refreshTimer();

private:
    static uint32_t s_redrawTimerID;

    KeyModifier _keyModifier{KeyModifier::None};
    std::array<bool, 0xff> _keyState{};
    SYSTEMTIME _lastSystemTime{};

    void registerCharacters(wchar_t wChar);
    void emitKeyEvent(bool isDown, uint8_t vKeyCode);
    uint64_t getElapsedTime();
};

}

#endif // GLCPL_VIEW_WIN32_H
