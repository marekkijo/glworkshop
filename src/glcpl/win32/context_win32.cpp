#include "context_win32.hpp"
#include "view_win32.hpp"
#include <windows.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/wglext.h>

static PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB{nullptr};
static PFNWGLGETPIXELFORMATATTRIBFVARBPROC wglGetPixelFormatAttribfvARB{nullptr};
static PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB{nullptr};
static PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB{nullptr};

namespace glcpl {

bool ContextWin32::s_wglExtensionsLoaded{false};

ContextWin32::ContextWin32(const ContextInfo &contextInfo) :
    Context(contextInfo) {
}

ContextWin32::~ContextWin32() {
    destroyOpenGLContext();
}

bool ContextWin32::makeCurrent() {
    return wglMakeCurrent(_hDC, _hGLRC) != NULL;
}

bool ContextWin32::swapBuffers() {
    return SwapBuffers(_hDC) != NULL;
}

bool ContextWin32::isValid() {
    return _hGLRC != nullptr;
}

bool ContextWin32::init(HWND hWnd) {
    _hWnd = hWnd;

    if (!s_wglExtensionsLoaded) {
        HDC basicHDC;
        HGLRC basicHGLRC;

        if (!createBasicGLContext(_hWnd, &basicHDC, &basicHGLRC))
            return false;

        bool wglResult = loadWGLExtensions();
        destroyBasicGLContext(_hWnd, &basicHDC, &basicHGLRC);

        if (!wglResult)
            return false;

        s_wglExtensionsLoaded = true;
    }

    createOpenGLContext();

    return true;
}

void ContextWin32::terminate() {
    destroyOpenGLContext();
}

bool ContextWin32::createOpenGLContext() {
    // TODO: Have to update by _contextInfo.samples: https://www.opengl.org/registry/specs/ARB/multisample.txt
    // TODO: Have to support debug context functionality: https://www.opengl.org/registry/specs/ARB/multisample.txt

    if (!(_hDC = GetDC(_hWnd)))
        return false;

    int doubleBuffer = _contextInfo.swapBufferType == ContextInfo::SwapBufferType::Double ?
        GL_TRUE : GL_FALSE;
    int piAttribIList[]{
        WGL_DRAW_TO_WINDOW_ARB,     GL_TRUE,
        WGL_ACCELERATION_ARB,       WGL_FULL_ACCELERATION_ARB,
        WGL_SWAP_METHOD_ARB,        WGL_SWAP_EXCHANGE_ARB,
        WGL_SUPPORT_OPENGL_ARB,     GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB,      doubleBuffer,
        WGL_PIXEL_TYPE_ARB,         WGL_TYPE_RGBA_ARB,
        WGL_COLOR_BITS_ARB,         _contextInfo.colorBufferSize,
        WGL_ACCUM_BITS_ARB,         _contextInfo.accumBufferSize,
        WGL_DEPTH_BITS_ARB,         _contextInfo.depthBufferSize,
        WGL_STENCIL_BITS_ARB,       _contextInfo.stencilBufferSize,
        GL_ZERO
    };

    float pfAttribFList[]{
        GL_ZERO
    };

    const unsigned int nMaxFormats = 1u;
    int piFormat;
    unsigned int nNumFormats;

    if (!wglChoosePixelFormatARB(_hDC, piAttribIList, pfAttribFList, nMaxFormats, &piFormat, &nNumFormats)) {
        destroyOpenGLContext();
        return false;
    }

    if (!nNumFormats || !SetPixelFormat(_hDC, piFormat, nullptr)) {
        destroyOpenGLContext();
        return false;
    }

    int contextFlags = _contextInfo.debugContext ? WGL_CONTEXT_DEBUG_BIT_ARB : 0;
    int contextProfileMask = _contextInfo.profile == ContextInfo::Profile::Core ?
        WGL_CONTEXT_CORE_PROFILE_BIT_ARB : WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB;
    int attribList[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB,  _contextInfo.getVersionMajor(),
        WGL_CONTEXT_MINOR_VERSION_ARB,  _contextInfo.getVersionMinor(),
        WGL_CONTEXT_FLAGS_ARB,          contextFlags,
        WGL_CONTEXT_PROFILE_MASK_ARB,   contextProfileMask,
        GL_ZERO
    };

    // TODO: Shared context
    _hGLRC = wglCreateContextAttribsARB(_hDC, nullptr /* HGLRC hShareContext */, attribList);

    if (!_hGLRC || !wglMakeCurrent(_hDC, _hGLRC)) {
        destroyOpenGLContext();
        return false;
    }

    return true;
}

void ContextWin32::destroyOpenGLContext() {
    if (_hDC) {
        wglMakeCurrent(_hDC, nullptr);
        if (_hGLRC)
            wglDeleteContext(_hGLRC);
        ReleaseDC(_hWnd, _hDC);
    }
    _hDC = nullptr;
    _hGLRC = nullptr;
}

bool ContextWin32::createBasicGLContext(const HWND &hWnd, HDC *hDC, HGLRC *hGLRC) {
    if (!(*hDC = GetDC(hWnd)))
        return false;

    DWORD dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

    PIXELFORMATDESCRIPTOR pfd{
        sizeof(PIXELFORMATDESCRIPTOR),  // WORD  nSize
        1ul,                            // WORD  nVersion
        dwFlags,                        // DWORD dwFlags
        PFD_TYPE_RGBA,                  // BYTE  iPixelType
        32u,                            // BYTE  cColorBits
        0u, 0u,                         // BYTE  cRed[Bits,Shift]
        0u, 0u,                         // BYTE  cGreen[Bits,Shift]
        0u, 0u,                         // BYTE  cBlue[Bits,Shift]
        0u, 0u,                         // BYTE  cAlpha[Bits,Shift]
        0u,                             // BYTE  cAccumBits
        0u, 0u, 0u, 0u,                 // BYTE  cAccum[Red,Green,Blue,Alpha]Bits
        24u,                            // BYTE  cDepthBits
        8u,                             // BYTE  cStencilBits
        0u,                             // BYTE  cAuxBuffers
        PFD_MAIN_PLANE,                 // BYTE  iLayerType
        0u,                             // BYTE  bReserved
        0ul, 0ul, 0ul                   // DWORD dw[Layer,Visible,Damage]Mask
    };

    int pfIndex = ChoosePixelFormat(*hDC, &pfd);
    if (!SetPixelFormat(*hDC, pfIndex, &pfd) ||
        !(*hGLRC = wglCreateContext(*hDC))) {
        destroyBasicGLContext(hWnd, hDC, nullptr);
        return false;
    }

    if (!wglMakeCurrent(*hDC, *hGLRC)) {
        destroyBasicGLContext(hWnd, hDC, hGLRC);
        return false;
    }

    return true;
}

void ContextWin32::destroyBasicGLContext(const HWND &hWnd, HDC *hDC, HGLRC *hGLRC) {
    if (hDC && *hDC) {
        wglMakeCurrent(*hDC, nullptr);
        if (hGLRC && *hGLRC) {
            wglDeleteContext(*hGLRC);
            *hGLRC = nullptr;
        }
        ReleaseDC(hWnd, *hDC);
        *hDC = nullptr;
    }
}

bool ContextWin32::loadWGLExtensions() {
    wglGetPixelFormatAttribivARB = reinterpret_cast<PFNWGLGETPIXELFORMATATTRIBIVARBPROC>(wglGetProcAddress("wglGetPixelFormatAttribivARB"));
    wglGetPixelFormatAttribfvARB = reinterpret_cast<PFNWGLGETPIXELFORMATATTRIBFVARBPROC>(wglGetProcAddress("wglGetPixelFormatAttribfvARB"));
    wglChoosePixelFormatARB = reinterpret_cast<PFNWGLCHOOSEPIXELFORMATARBPROC>(wglGetProcAddress("wglChoosePixelFormatARB"));
    wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(wglGetProcAddress("wglCreateContextAttribsARB"));

    if (!wglGetPixelFormatAttribivARB ||
        !wglGetPixelFormatAttribfvARB ||
        !wglChoosePixelFormatARB ||
        !wglCreateContextAttribsARB) {
        wglGetPixelFormatAttribivARB = nullptr;
        wglGetPixelFormatAttribfvARB = nullptr;
        wglChoosePixelFormatARB = nullptr;
        wglCreateContextAttribsARB = nullptr;
        return false;
    }

    return true;
}

}
