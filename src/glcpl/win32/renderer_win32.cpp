#include "renderer.hpp"
#include <windows.h>
#include <GL/gl.h>
#include <GL/wglext.h>

namespace glcpl {

void *Renderer::getGLProcedure(const std::string &procName) {
    void *ptr = wglGetProcAddress(procName.c_str());
    if (ptr == nullptr ||
        ptr == reinterpret_cast<void *>(0x1) ||
        ptr == reinterpret_cast<void *>(0x2) ||
        ptr == reinterpret_cast<void *>(0x3) ||
        ptr == reinterpret_cast<void *>(-1)) {
        HMODULE hModule = LoadLibraryA("opengl32.dll");
        ptr = GetProcAddress(hModule, procName.c_str());
    }

    return ptr;
}

}
