#include "view_win32.hpp"
#include <iostream>
#include <memory>
#include <algorithm>
#include <cassert>
#include "context_win32.hpp"
#include "renderer.hpp"

namespace glcpl {

uint32_t ViewWin32::s_redrawTimerID{0};

ViewWin32::ViewWin32(HWND hWnd /* = nullptr */) :
    _hWnd(hWnd) {
    RECT rect;
    if (GetWindowRect(_hWnd, &rect)) {
        _width = rect.right - rect.left;
        _height = rect.top = rect.bottom;
    }
}

ViewWin32::~ViewWin32() {
}

std::shared_ptr<Context> ViewWin32::createContext(const ContextInfo &contextInfo) {
    auto context = std::make_shared<ContextWin32>(contextInfo);
    if (!context->init(_hWnd))
        return nullptr;
    _contexts.push_back(context);
    return context;
}

bool ViewWin32::destroyContext(std::shared_ptr<Context> context) {
    auto contextWin32 = std::dynamic_pointer_cast<ContextWin32>(context);
    if (!contextWin32)
        return false;

    auto it = std::find(_contexts.begin(), _contexts.end(), contextWin32);
    if (it == _contexts.end())
        return false;

    _contexts.erase(it);

    contextWin32->terminate();

    return true;
}

void ViewWin32::attachRenderer(std::shared_ptr<Renderer> renderer) {
    _renderer = renderer;
    if (_renderer) {
        _renderer->attachEvent();
        _renderer->resizeEvent({_width, _height});
    }
}

void ViewWin32::setTimerInterval(uint32_t msec) {
    _msecTimerInterval = msec;
    refreshTimer();
}

void ViewWin32::removeTimerInterval() {
    _msecTimerInterval = -1;
    refreshTimer();
}

LRESULT ViewWin32::wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
    case WM_CHAR:
    case WM_SYSCHAR:
        registerCharacters(static_cast<wchar_t>(wParam));
        return 0;
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
        emitKeyEvent(true, static_cast<uint8_t>(wParam));
        return 0;
    case WM_KEYUP:
    case WM_SYSKEYUP:
        emitKeyEvent(false, static_cast<uint8_t>(wParam));
        return 0;
    case WM_TIMER:
        if (wParam == s_redrawTimerID) {
            uint64_t elapsed = getElapsedTime();
            refreshTimer();

            _renderer->timerEvent({elapsed});
            return 0;
        }
    case WM_SIZE:
        _width = LOWORD(lParam);
        _height = HIWORD(lParam);
        if (_renderer)
            _renderer->resizeEvent({_width, _height});
        return 0;
    case WM_DESTROY:
        for (auto &context : _contexts)
            context->terminate();
        _contexts.clear();
        _hWnd = nullptr;
        return 0;
    }
    return DefWindowProc(hWnd, msg, wParam, lParam);
}

void ViewWin32::refreshTimer() {
    if (!_hWnd)
        return;
    if (_msecTimerInterval >= 0) {
        SetTimer(_hWnd, s_redrawTimerID, static_cast<uint32_t>(_msecTimerInterval), nullptr);
        GetSystemTime(&_lastSystemTime);
    } else {
        KillTimer(_hWnd, s_redrawTimerID);
    }
}

void ViewWin32::registerCharacters(wchar_t wChar) {
    std::wcout << wChar << std::endl;
}

void ViewWin32::emitKeyEvent(bool isDown, uint8_t vKeyCode) {
    KeyCode keyCode = KeyCode::None;

    switch (vKeyCode) {
    case VK_SHIFT:
        if (isDown) {
            if (GetKeyState(VK_RSHIFT) & 0x8000) {
                keyCode = KeyCode::RightShift;
                _keyModifier |= KeyModifier::Shift;
            } else if (GetKeyState(VK_LSHIFT) & 0x8000) {
                keyCode = KeyCode::LeftShift;
                _keyModifier |= KeyModifier::Shift;
            }
        } else {
            if (!(GetKeyState(VK_RSHIFT) & 0x8000) && !(GetKeyState(VK_LSHIFT) & 0x8000)) {
                _keyModifier = _keyModifier & !KeyModifier::Shift;
            }
            if (!(GetKeyState(VK_RSHIFT) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::RightShift)]) {
                keyCode = KeyCode::RightShift;
                if (!(GetKeyState(VK_LSHIFT) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftShift)]) {
                    _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftShift)] = false;
                    _renderer->keyEvent({isDown, KeyCode::LeftShift, _keyModifier});
                }
            } else if (!(GetKeyState(VK_LSHIFT) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftShift)]) {
                keyCode = KeyCode::LeftShift;
            }
        }
        break;
    case VK_CONTROL:
        if (isDown) {
            if (GetKeyState(VK_RCONTROL) & 0x8000) {
                keyCode = KeyCode::RightControl;
                _keyModifier |= KeyModifier::Control;
            } else if (GetKeyState(VK_LCONTROL) & 0x8000) {
                keyCode = KeyCode::LeftControl;
                _keyModifier |= KeyModifier::Control;
            }
        } else {
            if (!(GetKeyState(VK_RCONTROL) & 0x8000) && !(GetKeyState(VK_LCONTROL) & 0x8000)) {
                _keyModifier = _keyModifier & !KeyModifier::Control;
            }
            if (!(GetKeyState(VK_RCONTROL) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::RightControl)]) {
                keyCode = KeyCode::RightControl;
                if (!(GetKeyState(VK_LCONTROL) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftControl)]) {
                    _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftControl)] = false;
                    _renderer->keyEvent({isDown, KeyCode::LeftControl, _keyModifier});
                }
            } else if (!(GetKeyState(VK_LCONTROL) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftControl)]) {
                keyCode = KeyCode::LeftControl;
            }
        }
        break;
    case VK_MENU:
        if (isDown) {
            if (GetKeyState(VK_RMENU) & 0x8000) {
                keyCode = KeyCode::RightAlt;
                _keyModifier |= KeyModifier::Alt;
            } else if (GetKeyState(VK_LMENU) & 0x8000) {
                keyCode = KeyCode::LeftAlt;
                _keyModifier |= KeyModifier::Alt;
            }
        } else {
            if (!(GetKeyState(VK_RMENU) & 0x8000) && !(GetKeyState(VK_LMENU) & 0x8000)) {
                _keyModifier = _keyModifier & !KeyModifier::Alt;
            }
            if (!(GetKeyState(VK_RMENU) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::RightAlt)]) {
                keyCode = KeyCode::RightAlt;
                if (!(GetKeyState(VK_LMENU) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftAlt)]) {
                    _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftAlt)] = false;
                    _renderer->keyEvent({isDown, KeyCode::LeftAlt, _keyModifier});
                }
            } else if (!(GetKeyState(VK_LMENU) & 0x8000) && _keyState[std::underlying_type_t<KeyCode>(KeyCode::LeftAlt)]) {
                keyCode = KeyCode::LeftAlt;
            }
        }
        break;
    case VK_CAPITAL: keyCode = KeyCode::CapsLock; break;
    case VK_ESCAPE: keyCode = KeyCode::Escape; break;
    case VK_SPACE: keyCode = KeyCode::Space; break;
    case VK_SNAPSHOT: keyCode = KeyCode::PrintScreen; break;
    case VK_PAUSE: keyCode = KeyCode::Pause; break;
    case VK_INSERT: keyCode = KeyCode::Insert; break;
    case VK_DELETE: keyCode = KeyCode::Delete; break;
    case VK_BACK: keyCode = KeyCode::Backspace; break;
    case VK_TAB: keyCode = KeyCode::Tab; break;
    case VK_RETURN: keyCode = KeyCode::Enter; break;
    case VK_LWIN: keyCode = KeyCode::LeftSpecial; break;
    case VK_RWIN: keyCode = KeyCode::RightSpecial; break;
    case VK_APPS: keyCode = KeyCode::Context; break;
    case VK_NUMLOCK: keyCode = KeyCode::NumLock; break;
    case VK_SCROLL: keyCode = KeyCode::ScrollLock; break;
    case VK_OEM_PLUS: keyCode = KeyCode::EqualSign; break;
    case VK_OEM_COMMA: keyCode = KeyCode::Comma; break;
    case VK_OEM_MINUS: keyCode = KeyCode::MinusSign; break;
    case VK_OEM_PERIOD: keyCode = KeyCode::Period; break;
    case VK_OEM_1: keyCode = KeyCode::Semicolon; break;
    case VK_OEM_2: keyCode = KeyCode::Slash; break;
    case VK_OEM_3: keyCode = KeyCode::GraveAccent; break;
    case VK_OEM_4: keyCode = KeyCode::OpeningBracket; break;
    case VK_OEM_5: keyCode = KeyCode::Backslash; break;
    case VK_OEM_6: keyCode = KeyCode::ClosingBracket; break;
    case VK_OEM_7: keyCode = KeyCode::SingleQuote; break;
    default:
        if (vKeyCode >= 0x41 && vKeyCode <= 0x5A) {
            keyCode = static_cast<KeyCode>(vKeyCode - 0x41 + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::A));
        } else if (vKeyCode >= 0x30 && vKeyCode <= 0x39) {
            keyCode = static_cast<KeyCode>(vKeyCode - 0x30 + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::No0));
        } else if (vKeyCode >= VK_F1 && vKeyCode <= VK_F12) {
            keyCode = static_cast<KeyCode>(vKeyCode - VK_F1 + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::F01));
        } else if ((vKeyCode >= VK_NUMPAD0 && vKeyCode <= VK_ADD)) {
            keyCode = static_cast<KeyCode>(vKeyCode - VK_NUMPAD0 + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::NumPad0));
        } else if ((vKeyCode >= VK_SUBTRACT && vKeyCode <= VK_DIVIDE)) {
            keyCode = static_cast<KeyCode>(vKeyCode - VK_SUBTRACT + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::NumPadMinusSign));
        } else if (vKeyCode >= VK_PRIOR && vKeyCode <= VK_HOME) {
            keyCode = static_cast<KeyCode>(vKeyCode - VK_PRIOR + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::PageUp));
        } else if (vKeyCode >= VK_LEFT && vKeyCode <= VK_DOWN) {
            keyCode = static_cast<KeyCode>(vKeyCode - VK_LEFT + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::ArrowLeft));
        } else {
            std::cerr << "Unexpected virtual key code: 0x" << std::hex << static_cast<unsigned int>(vKeyCode) << std::endl;
            assert(false);
            return;
        }
        break;
    }

    if (_keyState[std::underlying_type_t<KeyCode>(keyCode)] == isDown) {
        return;
    }

    _keyState[std::underlying_type_t<KeyCode>(keyCode)] = isDown;
    _renderer->keyEvent({isDown, keyCode, _keyModifier});
}

uint64_t ViewWin32::getElapsedTime() {
    SYSTEMTIME currentSystemTime;
    GetSystemTime(&currentSystemTime);
    FILETIME currentFileTime;
    SystemTimeToFileTime(&currentSystemTime, &currentFileTime);
    uint64_t currentUnix{0u};
    currentUnix |= currentFileTime.dwLowDateTime;
    currentUnix |= static_cast<uint64_t>(currentFileTime.dwHighDateTime) << 32;

    FILETIME lastFileTime;
    SystemTimeToFileTime(&_lastSystemTime, &lastFileTime);
    uint64_t lastUnix{0u};
    lastUnix |= lastFileTime.dwLowDateTime;
    lastUnix |= static_cast<uint64_t>(lastFileTime.dwHighDateTime) << 32;

    return (currentUnix - lastUnix) / 10000u;
}

}
