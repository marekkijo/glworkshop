#ifndef GLCPL_WINDOW_WIN32_H
#define GLCPL_WINDOW_WIN32_H

#pragma warning(disable : 4250)

#include <memory>
#include <cstdint>
#include <windows.h>
#include "window.hpp"
#include "view_win32.hpp"

namespace glcpl {

class WindowWin32 final : public ViewWin32, public Window {
public:
    WindowWin32(const WindowInfo &windowInfo);
    ~WindowWin32() override;

    int exec() override;

    bool init();

protected:
    LRESULT wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;

private:
    LRESULT wndInternalProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    static const std::string s_windowClassName;
    static uint32_t s_windowClassInstanceCounter;
    static bool s_windowClassRegistered;

    static LRESULT CALLBACK msgRouter(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    static void createWindowClass();
    static void destroyWindowClass();
};

}

#endif // GLCPL_WINDOW_WIN32_H
