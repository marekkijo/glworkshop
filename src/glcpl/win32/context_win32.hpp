#ifndef GLCPL_CONTEXT_WIN32_H
#define GLCPL_CONTEXT_WIN32_H

#include <memory>
#include <windows.h>
#include "context.hpp"

namespace glcpl {

class ViewWin32;

class ContextWin32 final : public virtual Context {
public:
    ContextWin32(const ContextInfo &contextInfo);
    ~ContextWin32() override;

    bool makeCurrent() override;
    bool swapBuffers() override;

    bool isValid() override;

    bool init(HWND hWnd);
    void terminate();

protected:
    HWND _hWnd{nullptr};
    HDC _hDC{nullptr};
    HGLRC _hGLRC{nullptr};

    bool createOpenGLContext();
    void destroyOpenGLContext();

    static bool s_wglExtensionsLoaded;

    static bool createBasicGLContext(const HWND &hWnd, HDC *hDC, HGLRC *hGLRC);
    static void destroyBasicGLContext(const HWND &hWnd, HDC *hDC, HGLRC *hGLRC);
    static bool loadWGLExtensions();
};

}

#endif // GLCPL_CONTEXT_WIN32_H
