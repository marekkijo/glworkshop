#include "window.hpp"

#ifdef _WIN32
# include "win32/window_win32.hpp"
#endif

namespace glcpl {

Window::Window(const WindowInfo &windowInfo) :
    _windowInfo{windowInfo} {
}

std::shared_ptr<Window> Window::create(const WindowInfo &windowInfo) {
#ifdef _WIN32
    auto window = std::make_shared<WindowWin32>(windowInfo);
    if (!window->init())
        return nullptr;
    return window;
#else
    return nullptr;
#endif
}

}
