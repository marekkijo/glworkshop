#ifndef GLCPL_EVENTS_H
#define GLCPL_EVENTS_H

#include <cstdint>
#include "common.hpp"
#include "keys.hpp"

namespace glcpl {

struct GLCPL_API ResizeEventArgs {
    ResizeEventArgs(uint32_t width, uint32_t height) :
        width(width),
        height(height) {
    }
    uint32_t width;
    uint32_t height;
};

struct GLCPL_API KeyEventArgs {
    KeyEventArgs(bool isDown, KeyCode keyCode, KeyModifier keyModifier) :
        isDown(isDown),
        keyCode(keyCode),
        keyModifier(keyModifier) {
    }
    bool isDown{false}; // true - indicates that key has been pressed
                        // false - indicates that key has been released
    KeyCode keyCode{KeyCode::None};
    KeyModifier keyModifier{KeyModifier::None};
};

struct GLCPL_API TimerEventArgs {
    TimerEventArgs(uint64_t elapsed) :
        elapsed(elapsed) {
    }
    uint64_t elapsed{0u};   // elapsed miliseconds till last timer event;
};

}

#endif // GLCPL_EVENTS_H
