#ifndef GLCPL_RENDERER_H
#define GLCPL_RENDERER_H

#pragma warning(disable : 4251)

#include <memory>
#include <string>
#include "common.hpp"
#include "events.hpp"

namespace glcpl {

class Context;

class GLCPL_API Renderer {
public:
    explicit Renderer(std::shared_ptr<Context> context);
    virtual ~Renderer() { };

    virtual void attachEvent() = 0;
    virtual void resizeEvent(const ResizeEventArgs &args) = 0;
    virtual void keyEvent(const KeyEventArgs &args) = 0;
    virtual void timerEvent(const TimerEventArgs &args) = 0;

    static void *getGLProcedure(const std::string &procName);

protected:
    std::shared_ptr<Context> _context;
};

}

#endif // GLCPL_RENDERER_H
