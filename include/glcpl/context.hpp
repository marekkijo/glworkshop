#ifndef GLCPL_CONTEXT_H
#define GLCPL_CONTEXT_H

#include "common.hpp"
#include "contextinfo.hpp"

namespace glcpl {

class GLCPL_API Context {
public:
    virtual ~Context() { }

    virtual bool makeCurrent() = 0;
    virtual bool swapBuffers() = 0;

    virtual bool isValid() = 0;

protected:
    ContextInfo _contextInfo;

    Context(const ContextInfo &contextInfo);
};

}

#endif // GLCPL_CONTEXT_H
