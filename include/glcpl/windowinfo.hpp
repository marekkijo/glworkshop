#ifndef GLCPL_WINDOWINFO_H
#define GLCPL_WINDOWINFO_H

#pragma warning(disable : 4251)

#include <cstdint>
#include <string>
#include "common.hpp"

namespace glcpl {

struct GLCPL_API WindowInfo {
    std::string name;
    uint32_t width{640u};
    uint32_t height{480u};
    uint32_t posx{0u};
    uint32_t posy{0u};
    bool centered{true};
    bool exitOnEscape{true};
    bool cursorVisible{false};
    bool cursorCentering{true};

    enum class Type {
        SystemBorder,
        Borderless,
        Fullscreen
    } type{Type::SystemBorder};
};

}

#endif // GLCPL_WINDOWINFO_H
