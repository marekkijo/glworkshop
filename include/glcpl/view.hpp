#ifndef GLCPL_VIEW_H
#define GLCPL_VIEW_H

#include <memory>

#ifdef _WIN32
# include <windows.h>
#endif

#include "common.hpp"
#include "contextinfo.hpp"

namespace glcpl {

class Context;
class Renderer;

class GLCPL_API View {
public:
    virtual ~View() { }

    virtual std::shared_ptr<Context> createContext(const ContextInfo &contextInfo = ContextInfo()) = 0;
    virtual bool destroyContext(std::shared_ptr<Context> context) = 0;
    virtual void attachRenderer(std::shared_ptr<Renderer> renderer) = 0;
    virtual void setTimerInterval(uint32_t msec) = 0;
    virtual void removeTimerInterval() = 0;

#ifdef _WIN32
    virtual LRESULT wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;
    static std::shared_ptr<View> create(HWND hWnd);
#else
    static std::shared_ptr<View> create();
#endif

protected:
    View();
};

}

#endif // GLCPL_VIEW_H
