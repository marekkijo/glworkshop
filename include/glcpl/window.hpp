#ifndef GLCPL_WINDOW_H
#define GLCPL_WINDOW_H

#include <memory>
#include "common.hpp"
#include "view.hpp"
#include "windowinfo.hpp"

namespace glcpl {

class GLCPL_API Window : public virtual View {
public:
    virtual ~Window() { };

    virtual int exec() = 0;

    static std::shared_ptr<Window> create(const WindowInfo &windowInfo = WindowInfo());

protected:
    WindowInfo _windowInfo;

#ifdef _WIN32
    virtual LRESULT wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;
#endif

    Window(const WindowInfo &windowInfo);
};

}

#endif // GLCPL_WINDOW_H
