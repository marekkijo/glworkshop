#ifndef GLCPL_COMMON_H
#define GLCPL_COMMON_H

#ifdef _WIN32
# ifdef GLCPL_EXPORT
#  define GLCPL_API __declspec(dllexport)
#  define GLCPL_API_EXTERN
# else
#  define GLCPL_API __declspec(dllimport)
#  define GLCPL_API_EXTERN extern
# endif
#endif

#endif // GLCPL_COMMON_H
