#ifndef GLCPL_KEYS_H
#define GLCPL_KEYS_H

#include "common.hpp"

namespace glcpl {

enum class GLCPL_API KeyCode : uint8_t {
    None    = 0x00,
    F01     = 0x01,
    F02     = 0x02,
    F03     = 0x03,
    F04     = 0x04,
    F05     = 0x05,
    F06     = 0x06,
    F07     = 0x07,
    F08     = 0x08,
    F09     = 0x09,
    F10     = 0x0A,
    F11     = 0x0B,
    F12     = 0x0C,
    No0     = 0x30,
    No1     = 0x31,
    No2     = 0x32,
    No3     = 0x33,
    No4     = 0x34,
    No5     = 0x35,
    No6     = 0x36,
    No7     = 0x37,
    No8     = 0x38,
    No9     = 0x39,
    MinusSign       = 0x3A,
    EqualSign       = 0x3B,
    OpeningBracket  = 0x3C,
    ClosingBracket  = 0x3D,
    Backslash       = 0x3E,
    Semicolon       = 0x3F,
    SingleQuote     = 0x40,
    Comma           = 0x41,
    Period          = 0x42,
    Slash           = 0x43,
    GraveAccent     = 0x44,
    Space           = 0x45,
    Backspace       = 0x46,
    Tab             = 0x47,
    Enter           = 0x48,
    Escape          = 0x49,
    PageUp          = 0x4A,
    PageDown        = 0x4B,
    End             = 0x4C,
    Home            = 0x4D,
    Insert          = 0x4E,
    Delete          = 0x4F,
    ArrowLeft       = 0x50,
    ArrowUp         = 0x51,
    ArrowRight      = 0x52,
    ArrowDown       = 0x53,
    PrintScreen     = 0x54,
    Pause           = 0x55,
    Context         = 0x56,
    LeftShift       = 0x57,
    RightShift      = 0x58,
    LeftControl     = 0x59,
    RightControl    = 0x5A,
    LeftAlt         = 0x5B,
    RightAlt        = 0x5C,
    LeftSpecial     = 0x5D,
    RightSpecial    = 0x5E,
    A       = 0x61,
    B       = 0x62,
    C       = 0x63,
    D       = 0x64,
    E       = 0x65,
    F       = 0x66,
    G       = 0x67,
    H       = 0x68,
    I       = 0x69,
    J       = 0x6A,
    K       = 0x6B,
    L       = 0x6C,
    M       = 0x6D,
    N       = 0x6E,
    O       = 0x6F,
    P       = 0x70,
    Q       = 0x71,
    R       = 0x72,
    S       = 0x73,
    T       = 0x74,
    U       = 0x75,
    V       = 0x76,
    W       = 0x77,
    X       = 0x78,
    Y       = 0x79,
    Z       = 0x7A,
    NumPad0         = 0x80,
    NumPad1         = 0x81,
    NumPad2         = 0x82,
    NumPad3         = 0x83,
    NumPad4         = 0x84,
    NumPad5         = 0x85,
    NumPad6         = 0x86,
    NumPad7         = 0x87,
    NumPad8         = 0x88,
    NumPad9         = 0x89,
    NumPadMulSign   = 0x8A, // *
    NumPadPlusSign  = 0x8B, // +
    NumPadMinusSign = 0x8C, // -
    NumPadDot       = 0x8D, // .
    NumPadDivSign   = 0x8E, // /
    CapsLock        = 0x90,
    NumLock         = 0x91,
    ScrollLock      = 0x92
};

enum class GLCPL_API KeyModifier : uint8_t {
    None    = 0x00,
    Shift   = 0x01,
    Control = 0x02,
    Alt     = 0x04
};

using T = std::underlying_type_t<KeyModifier>;

inline KeyModifier operator|(KeyModifier lhs, KeyModifier rhs) {
    return static_cast<KeyModifier>(static_cast<T>(lhs) | static_cast<T>(rhs));
}

inline KeyModifier& operator|=(KeyModifier& lhs, KeyModifier rhs) {
    lhs = static_cast<KeyModifier>(static_cast<T>(lhs) | static_cast<T>(rhs));
    return lhs;
}

inline KeyModifier operator&(KeyModifier lhs, KeyModifier rhs) {
    return static_cast<KeyModifier>(static_cast<T>(lhs) & static_cast<T>(rhs));
}

inline KeyModifier& operator&=(KeyModifier& lhs, KeyModifier rhs) {
    lhs = static_cast<KeyModifier>(static_cast<T>(lhs) & static_cast<T>(rhs));
    return lhs;
}
inline KeyModifier operator!(KeyModifier lhs) {
    return static_cast<KeyModifier>(!static_cast<T>(lhs));
}

}

#endif // GLCPL_KEYS_H
