#ifndef GLCPL_CONTEXTINFO_H
#define GLCPL_CONTEXTINFO_H

#include <cstdint>
#include "common.hpp"

namespace glcpl {

struct GLCPL_API ContextInfo {
    uint32_t swapInterval{0u};
    uint8_t samples{0u};
    uint8_t colorBufferSize{32u};
    uint8_t accumBufferSize{0u};
    uint8_t depthBufferSize{24u};
    uint8_t stencilBufferSize{8u};
    bool debugContext = false;

    enum class Version : std::uint16_t {
        Undefined = 0x0000,
        Highest = 0xFFFF,
        V4_5 = 0x0405,
        V4_4 = 0x0404,
        V4_3 = 0x0403,
        V4_2 = 0x0402,
        V4_1 = 0x0401,
        V4_0 = 0x0400,
        V3_3 = 0x0303,
        V3_2 = 0x0302,
        V3_1 = 0x0301,
        V3_0 = 0x0300,
        V2_1 = 0x0201,
        V2_0 = 0x0200,
        V1_5 = 0x0105,
        V1_4 = 0x0104,
        V1_3 = 0x0103,
        V1_2 = 0x0102,
        V1_1 = 0x0101
    } version{Version::Highest};
    uint8_t getVersionMajor() const;
    uint8_t getVersionMinor() const;
    static Version toVersion(uint8_t major, uint8_t minor);

    enum class Profile {
        Undefined,
        Core,
        Compatibility
    } profile{Profile::Core};

    enum class SwapBufferType {
        Undefined,
        Single,
        Double
    } swapBufferType{SwapBufferType::Double};
};

}

#endif // GLCPL_CONTEXTINFO_H
